import re


with open("input.txt", "r") as f:
    line = f.read()
    data_list = re.split(r"\n", line)

m = data_list[-2]
m = int(m)
data_list = data_list[:-2]
dict = {}

for data in data_list:
    rep = re.split(r":", data)
    dict[rep[0]] = rep[1]

    output = ""

for i in range(m):
    if str(i) in dict.keys():
        if m % i == 0:
            output = output + dict[str(i)]
if output == "":
    output = m

print(output)
